from django.urls import reverse
from django.http import HttpResponseNotFound, HttpResponseRedirect, Http404
from django.shortcuts import render

# Create your views here.

months_with_challenges = {
    "january": "Eat no meat for the entire month!",
    "february": "Walk for at least 5km a day!",
    "march": "Learn Django for at least 2 hours a day!",
    "april": "Eat no meat for the entire month!",
    "may": "Walk for at least 5km a day!",
    "june": "Learn Django for at least 2 hours a day!",
    "july": "Eat no meat for the entire month!",
    "august": "Walk for at least 5km a day!",
    "september": "Learn Django for at least 2 hours a day!",
    "october": "Eat no meat for the entire month!",
    "november": "Walk for at least 5km a day!",
    "december": "Learn Django for at least 2 hours a day!"
}


def monthly_challenge_by_number(request, month):
    months = list(months_with_challenges.keys())
    if month > len(months):
        raise Http404()

    redirect_month = months[month - 1]
    redirect_path = reverse("monthly-challenges", args=[redirect_month])  # */challenge/january
    return HttpResponseRedirect(redirect_path)


def monthly_challenge(request, month):
    if month not in months_with_challenges.keys():
        raise Http404()

    challenge_text = months_with_challenges[month]

    return render(request, "challenges/challenge.html", {
        "challenge_text": challenge_text,
        "month": month
    })


def months(request):
    months = list(months_with_challenges.keys())

    return render(request, "challenges/index.html", {
        "months": months
    })
